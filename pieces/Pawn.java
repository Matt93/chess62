package pieces;

import board.Square;

public class Pawn extends Piece {

	public String name = "P";
	public boolean firstmove = true;
	//public boolean firstmove = true;
	public Pawn(Color c){
		super(c);
	}

	/**
	 * returns name of the piece
	 */

	public String toString() {
		return super.toString() + name;
	}

	/**
	 * @return
	 * 		returns the type of piece(knight,pawn,rook,etc...)
	 *
	 */

	public String whatisPiece(){
		return "pawn";
	}

	/**
	 *
	 * @param startingsquare
	 * 				the starting point for this piece
	 * @param endingsquare
	 * 				ending point for this piece
	 *
	 * returns true if it's a legal move
	 */

	public boolean validateMove(Square startingSquare, Square endingSquare) {
		if (!hasMoved) {
			if (endingSquare.getXCoordinate() == startingSquare.getXCoordinate()){
				if (endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 2 ||
					endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 2)
					return true;
				else if (endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 1 ||
						 endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 1)
					return true;
			}
		} else if (endingSquare.getXCoordinate() == startingSquare.getXCoordinate()) {
			System.out.println("in here");
			if (this.color == Color.WHITE) {
				if (endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 1)
					return true;
			} else
				if (endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 1)
					return true;
			// pawn capturing normally
		}
		else if(enpass){
			if ((endingSquare.getXCoordinate() == startingSquare.getXCoordinate() + 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 1 ||
					 endingSquare.getXCoordinate() == startingSquare.getXCoordinate() + 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 1 ||
					 endingSquare.getXCoordinate() == startingSquare.getXCoordinate() - 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 1 ||
					 endingSquare.getXCoordinate() == startingSquare.getXCoordinate() - 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 1)){

					return true;
			}
		}
		else if (capturing) {
			if ((endingSquare.getXCoordinate() == startingSquare.getXCoordinate() + 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 1 ||
				 endingSquare.getXCoordinate() == startingSquare.getXCoordinate() + 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 1 ||
				 endingSquare.getXCoordinate() == startingSquare.getXCoordinate() - 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 1 ||
				 endingSquare.getXCoordinate() == startingSquare.getXCoordinate() - 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 1) )
				return true;
		}
		return false;
	}
}