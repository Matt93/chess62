package pieces;

import board.Square;

public class Knight extends Piece{

	public String name = "N";

	public Knight(Color c){
		super(c);
	}

	/**
	 * returns name of the piece
	 */

	public String toString() {
		return super.toString() + name;
	}

	/**
	 * @return
	 * 		returns the type of piece(knight,pawn,rook,etc...)
	 *
	 */

	public String whatisPiece(){
		return "knight";
	}

	/**
	 *
	 * @param startingsquare
	 * 				the starting point for this piece
	 * @param endingsquare
	 * 				ending point for this piece
	 *
	 * returns true if it's a legal move
	 */

	public boolean validateMove(Square startingSquare, Square endingSquare) {

		if (endingSquare.getXCoordinate() == startingSquare.getXCoordinate() + 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 2 ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() + 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 2 ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() + 2 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 1 ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() + 2 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 1 ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() - 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 2 ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() - 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 2 ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() - 2 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 1 ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() - 2 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 1) {
			return true;
		} else
			return false;
	}

}