package pieces;

import board.Square;

public abstract class Piece {

	public Color color;
	public boolean hasMoved = false;
	public boolean capturing = false;
	public boolean promoting = false;
	public boolean firstmove = true;
	public boolean enpass = false;

	public Piece(Color c){
		this.color = c;
	}

	/**
	 * @return
	 * 		returns the type of piece(knight,pawn,rook,etc...)
	 *
	 */

	public String whatisPiece(){
		return "";
	}

	/**
	 * returns color of the piece of type String
	 */


	public String toString() {
		if (color == Color.WHITE){
			return "w";
		} else
			return "b";
	}

	/**
	 * the superclass method of validatemove
	 */
	public abstract boolean validateMove(Square startingSquare, Square endingSquare);
}