package pieces;

import board.Square;

public class Bishop extends Piece {

	public String name = "B";

	public Bishop(Color c){
		super(c);
	}

	/**
	 * returns name of the piece
	 */

	public String toString() {
		return super.toString() + name;
	}

	/**
	 *
	 * @param startingsquare
	 * 				the starting point for this piece
	 * @param endingsquare
	 * 				ending point for this piece
	 *
	 * returns true if it's a legal move
	 */

	public boolean validateMove(Square startingSquare, Square endingSquare) {
		if (endingSquare.getXCoordinate() - startingSquare.getXCoordinate() != 0) {
			if ((endingSquare.getYCoordinate() - startingSquare.getYCoordinate()) / (endingSquare.getXCoordinate() - startingSquare.getXCoordinate()) == 1 ||
				(endingSquare.getYCoordinate() - startingSquare.getYCoordinate()) / (endingSquare.getXCoordinate() - startingSquare.getXCoordinate()) == -1) {
				return true;
			}
		}
		return false;
	}
}