package pieces;

import board.Square;

public class Queen extends Piece{

	public String name = "Q";

	public Queen(Color c){
		super(c);
	}

	/**
	 * returns name of the piece
	 */

	public String toString() {
		return super.toString() + name;
	}

	/**
	 *
	 * @param startingsquare
	 * 				the starting point for this piece
	 * @param endingsquare
	 * 				ending point for this piece
	 *
	 * returns true if it's a legal move
	 */

	public boolean validateMove(Square startingSquare, Square endingSquare) {
		if (endingSquare.getXCoordinate() - startingSquare.getXCoordinate() != 0 && !(endingSquare.getXCoordinate() == startingSquare.getXCoordinate() || endingSquare.getYCoordinate() == startingSquare.getYCoordinate())) {
			if ((endingSquare.getYCoordinate() - startingSquare.getYCoordinate()) / (endingSquare.getXCoordinate() - startingSquare.getXCoordinate()) == 1 ||
				(endingSquare.getYCoordinate() - startingSquare.getYCoordinate()) / (endingSquare.getXCoordinate() - startingSquare.getXCoordinate()) == -1) //||
					return true;
		} else if (endingSquare.getXCoordinate() == startingSquare.getXCoordinate() ||
				   endingSquare.getYCoordinate() == startingSquare.getYCoordinate()) {
			return true;
		}
		return false;
	}
}
