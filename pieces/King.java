package pieces;

import board.Square;

public class King extends Piece {

	public String name = "K";

	public King(Color c){
		super(c);
	}
	/**
	 * returns name of this piece
	 */
	public String toString() {
		return super.toString() + name;
	}

	/**
	 * @return
	 * 		returns the type of piece(knight,pawn,rook,etc...)
	 *
	 */

	public String whatisPiece(){
		return "king";
	}

	/**
	 *checks to see if it's a legal king move
	 */

	public boolean validateMove(Square startingSquare, Square endingSquare) {

		if (endingSquare.getXCoordinate() == startingSquare.getXCoordinate() + 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 1 ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() + 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate()     ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() + 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 1 ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 1     ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 1     ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() - 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() + 1 ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() - 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate()     ||
			endingSquare.getXCoordinate() == startingSquare.getXCoordinate() - 1 && endingSquare.getYCoordinate() == startingSquare.getYCoordinate() - 1 ) {
			return true;
		} else
			return false;
	}

}