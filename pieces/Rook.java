package pieces;

import board.Square;

public class Rook extends Piece{

	public String name = "R";

	public Rook(Color c){
		super(c);
	}

	/**
	 * returns name of the piece
	 */

	public String toString() {
		return super.toString() + name;
	}

	public String whatisPiece(){
		return "rook";
	}

	/**
	 *
	 * @param startingsquare
	 * 				the starting point for this piece
	 * @param endingsquare
	 * 				ending point for this piece
	 *
	 * returns true if it's a legal move
	 */
	public boolean validateMove(Square startingSquare, Square endingSquare) {

		if (endingSquare.getXCoordinate() == startingSquare.getXCoordinate() ||
			endingSquare.getYCoordinate() == startingSquare.getYCoordinate()) {
			return true;
		} else
			return false;
	}

}