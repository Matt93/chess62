package board;

import pieces.*;

public class ChessBoard {
	private int numberOfRanks = 8;
	private int numberOfFiles = 8;
	
	public Square board[][];
	public int moveNumber = 1;
	public boolean blackTurn = false;
	public boolean drawFlag = false;
	public boolean gameEnded = false;
	
	public ChessBoard() {
		board = new Square[numberOfRanks][numberOfFiles];
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++){
				board[i][j] = new Square(i, j, null);
			}
		}
		
		this.loadStandardSetup();
	}
	
	private void loadStandardSetup(){	
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if (i == 0 || i == 7) {
					if (j == 0 || j == 7) {
						if (i >= 6)
							board[i][j].piece = new Rook(Color.BLACK);
						else
							board[i][j].piece = new Rook(Color.WHITE);
					} else if (j == 1 || j == 6) {
						if (i >= 6)
							board[i][j].piece = new Knight(Color.BLACK);
						else
							board[i][j].piece = new Knight(Color.WHITE);
					} else if (j == 2 || j == 5) {
						if (i >= 6)
							board[i][j].piece = new Bishop(Color.BLACK);
						else
							board[i][j].piece = new Bishop(Color.WHITE);
					} else if (j == 3) {
						if (i >= 6)
							board[i][j].piece = new King(Color.BLACK);
						else
							board[i][j].piece = new King(Color.WHITE);
					} else if (j == 4) {
						if (i >= 6)
							board[i][j].piece = new Queen(Color.BLACK);
						else
							board[i][j].piece = new Queen(Color.WHITE);
					}
				} else if (i == 1 || i == 6) {
					if (i >= 6)
						board[i][j].piece = new Pawn(Color.BLACK);
					else
						board[i][j].piece = new Pawn(Color.WHITE);
				}
			}
		}
	}
	
	public void takeInput(String input) {
		int rankInt1;
		int fileInt1;
		int rankInt2;
		int fileInt2;
		
		if (input.equals("resign")) {
			this.resign();
		} else if (input.length() == 5) {
			char file1 = input.charAt(0);
			char rank1 = input.charAt(1);
			char file2 = input.charAt(3);
			char rank2 = input.charAt(4);
			
			rankInt1 = Integer.parseInt(rank1 + "") - 1;
			rankInt2 = Integer.parseInt(rank2 + "") - 1;

			fileInt1 = Character.toLowerCase(reverseLetters(file1)) - 'a';
			fileInt2 = Character.toLowerCase(reverseLetters(file2)) - 'a';
			
			if (input.equals("e1 g1") || input.equals("e1 b1") || input.equals("e8 b8") || input.equals("e8 g8")) {
				this.castle(rankInt1, fileInt1, rankInt2, fileInt2);
			} else
				this.checkMove(rankInt1, fileInt1, rankInt2, fileInt2);
		} else if (input.equals("draw")) {
			if (this.drawFlag) {
				this.draw();
			} else {
				System.out.println("Invalid input.");
				return;
			}
		} else if (input.substring(6).equals("draw?")) {
			this.drawFlag = true;
			this.takeInput(input.substring(0,5));
		} else {
			System.out.println("Invalid input.");
			return;
		}
	}
	
	public void checkMove(int rankInt1, int fileInt1, int rankInt2, int fileInt2, String piece){
		Square startingSquare = this.board[rankInt1][fileInt1];
		Square endingSquare = this.board[rankInt2][fileInt2];
		Piece movingPiece = this.board[rankInt1][fileInt1].piece;
		Piece takenPiece = this.board[rankInt2][fileInt2].piece;

		if (startingSquare == endingSquare) {
			System.out.println("Illegal move, try again.");
		} else if (movingPiece == null) {
			System.out.println("Illegal move, try again.");
		} else if (movingPiece.color == Color.BLACK && blackTurn == false || movingPiece.color == Color.WHITE && blackTurn == true) {
			System.out.println("Illegal move, try again.");
		} else if (this.pieceBlocking(rankInt1, fileInt1, rankInt2, fileInt2)) {
			System.out.println("Illegal move, try again.");
		} else if (takenPiece != null && (takenPiece.color == Color.BLACK && blackTurn == true) ||
				   takenPiece != null && (takenPiece.color == Color.WHITE && blackTurn == false)) {
			System.out.println("Illegal move, try again.");
		} else {
			if (takenPiece != null) {
					movingPiece.capturing = true;
			}


			if (movingPiece.validateMove(startingSquare, endingSquare)) {

				if(movingPiece.whatisPiece().equals("pawn")){

						enpassant(rankInt1,fileInt1,rankInt2,fileInt2);
				}

				this.move(rankInt1, fileInt1, rankInt2, fileInt2);

				if (this.pawnOn1stOr8thRank(endingSquare)) {
					this.pawnPromotion(endingSquare, piece);
				}
			} else
				System.out.println("Illegal move, try again.");
		}
	}
	
	public void move(int rankInt1, int fileInt1, int rankInt2, int fileInt2) {


		int pieceleft = fileInt1 + 1;
		int pieceright = fileInt1 - 1;
		int moveup = Math.abs(rankInt1-rankInt2);
		int oneup = rankInt1 + moveup;
		int onedown = rankInt1 - moveup;


		if(this.board[rankInt1][fileInt1].piece.enpass==true){
			//System.out.println("made it pass enpass");
			if(moveup==1){

				if(this.board[rankInt1][fileInt1].piece.color==Color.WHITE){
					if(this.board[rankInt1][pieceleft]!=null && this.board[rankInt1][pieceleft].piece!=null){
						if(rankInt2==oneup && pieceleft==fileInt2){
							this.board[rankInt1][pieceleft].piece = null;
						}
					}
				else if(this.board[rankInt1][pieceright]!=null && this.board[rankInt1][pieceright].piece!=null){
					if(rankInt2==oneup && pieceright==fileInt2){
						this.board[rankInt1][pieceright].piece = null;
						}
					}
				}
				else{
					if(this.board[rankInt1][pieceleft]!=null && this.board[rankInt1][pieceleft].piece!=null){
						if(rankInt2==onedown && pieceleft==fileInt2){
							this.board[rankInt1][pieceleft].piece = null;
						}
					}
					else if(this.board[rankInt1][pieceright]!=null && this.board[rankInt1][pieceright].piece!=null){
					//System.out.println("made it past third level");
					//System.out.println("rankint2 is " + rankInt2);
					//System.out.println("oneup is " + onedown);
					//System.out.println("pieceright is " + pieceright);
					//System.out.println("fileint2 is " + fileInt2);
						if(rankInt2==onedown && pieceright==fileInt2){
							this.board[rankInt1][pieceright].piece = null;
						}
				}

			}

			}
		}
		this.board[rankInt2][fileInt2].piece = this.board[rankInt1][fileInt1].piece;
		this.board[rankInt1][fileInt1].piece = null;
		this.board[rankInt2][fileInt2].piece.hasMoved = true;

		this.moveNumber++;
		this.blackTurn = (moveNumber % 2 == 0);

	}
	
	public boolean pieceBlocking(int rankInt1, int fileInt1, int rankInt2, int fileInt2) {
		
		if (rankInt1 == rankInt2) {
			if (fileInt1 > fileInt2) {
				for (int i = fileInt1 + 1; i < fileInt2; i++) {
					if (this.board[rankInt1][i].isOccupied())
						return true;
				}
			} else
				for (int i = fileInt1 - 1; i > fileInt2; i--) {
					if (this.board[rankInt1][i].isOccupied())
						return true;
				}
		} else if (fileInt1 == fileInt2) {
			if (rankInt1 > rankInt2) {
				for (int i = rankInt1 - 1; i > rankInt2; i--) {
					if (this.board[i][fileInt1].isOccupied())
						return true;
				}
			} else
				for (int i = rankInt1 + 1; i < rankInt2; i++) {
					if (this.board[i][fileInt1].isOccupied())
						return true;
				}
		} else if ((fileInt2 - fileInt1) / (rankInt2 - rankInt1) == 1 || (fileInt2 - fileInt1) / (rankInt2 - rankInt1) == -1) {
			if (rankInt1 + 1 == rankInt2 && fileInt1 + 1 == fileInt2 ||
				rankInt1 + 1 == rankInt2 && fileInt1 - 1 == fileInt2 ||
				rankInt1 - 1 == rankInt2 && fileInt1 + 1 == fileInt2 ||
				rankInt1 - 1 == rankInt2 && fileInt1 + 1 == fileInt2 ) {
				return false;
			} else {
				int changeInRankInt;
				int changeInFileInt;
				int currentRankInt = rankInt1;
				int currentFileInt = fileInt1;
				
				if (rankInt1 > rankInt2) {
					changeInRankInt = -1;
				} else {
					changeInRankInt = 1;
				}
				
				if (fileInt1 > fileInt2) {
					changeInFileInt = -1;
				} else {
					changeInFileInt = 1;
				}
			
				currentRankInt = currentRankInt + changeInRankInt;
				currentFileInt = currentFileInt + changeInFileInt;
				
				while(currentRankInt != rankInt2 && currentFileInt != fileInt2) {
					if (this.board[currentRankInt][currentFileInt].isOccupied()) {
						return true;
					}
					
					currentRankInt = currentRankInt + changeInRankInt;
					currentFileInt = currentFileInt + changeInFileInt;
				}
			}
		}
		return false;
	}
	
	public boolean capture(int rankInt1, int fileInt1,int rankInt2, int fileInt2){

		if(this.board[rankInt1][fileInt1].piece.whatisPiece().equals("pawn")){
			if(this.board[rankInt1][fileInt1].piece.color.equals(Color.WHITE)){
				if(fileInt1==0){
					if(this.board[rankInt1+1][fileInt1+1].isOccupied() && this.board[rankInt1+1][fileInt1+1].piece.color.equals(Color.BLACK)){
						return true;
					}
				}

				else if(fileInt2==(this.board[0].length-1)){
					if(this.board[rankInt1+1][fileInt1-1].isOccupied() && this.board[rankInt1+1][fileInt1-1].piece.color.equals(Color.BLACK)){
						return true;
					}
				}

				else {

				if(this.board[rankInt1+1][fileInt1+1].isOccupied() && this.board[rankInt1+1][fileInt1+1].piece.color.equals(Color.BLACK) || this.board[rankInt1+1][fileInt1-1].isOccupied() && this.board[rankInt1+1][fileInt1-1].piece.color.equals(Color.BLACK)){
					return true;
					}
				}
			}

			if(this.board[rankInt1][fileInt1].piece.color.equals(Color.BLACK)){
				if(fileInt1==0){
					if(this.board[rankInt1-1][fileInt1+1].isOccupied() && this.board[rankInt1-1][fileInt1+1].piece.color.equals(Color.WHITE)){
						return true;
					}
				}

				else if(fileInt2==(this.board[0].length-1)){
					if(this.board[rankInt1-1][fileInt1-1].isOccupied() && this.board[rankInt1-1][fileInt1-1].piece.color.equals(Color.WHITE)){
						return true;
					}
				}

				else {

				if(this.board[rankInt1+1][fileInt1+1].isOccupied() && this.board[rankInt1+1][fileInt1+1].piece.color.equals(Color.BLACK) || this.board[rankInt1+1][fileInt1-1].isOccupied() && this.board[rankInt1+1][fileInt1-1].piece.color.equals(Color.BLACK)){
					return true;
					}
				}
			}
		}

		if(this.board[rankInt1][fileInt1].piece.whatisPiece().equals("rook")){
			if(this.board[rankInt1][fileInt1].piece.whatisPiece().equals("rook")){
				if(this.board[rankInt1][fileInt1].piece.color.equals(Color.WHITE)){
					int rookmove1 = rankInt1 - rankInt2;
					int rookmove2 = fileInt1 - fileInt2;

					if(Math.abs(rookmove1)==Math.abs(rookmove2)){

					}

			}
		}

		}
		return false;
	}
	
	
	public void enpassant(int rankInt1, int fileInt1, int rankInt2, int fileInt2){
		if(this.board[rankInt1][fileInt1].piece.whatisPiece().equals("pawn")){
			int moving = rankInt1-rankInt2;
			//int sum = rankInt1 + 2;
			int sum2 = fileInt1 + 1;
			int sum3 = fileInt1 - 1;
			System.out.println("fileInt1 is " + fileInt1);
			//System.out.println("diff is  " + diff);
			if(Math.abs(moving)==2){
				if(fileInt1==0){
					if(this.board[rankInt2][sum2].piece!=null){
						//System.out.println("in here");
						//this.board[rankInt1][fileInt1].piece.enpass = true;
						this.board[rankInt2][sum2].piece.enpass = true;
					}
				}

				else if(fileInt1==(this.board[0].length-1)){
					if(this.board[rankInt2][sum3].piece!=null){
						//System.out.println("in here");
						//this.board[rankInt1][fileInt1].piece.enpass = true;
						this.board[rankInt2][sum3].piece.enpass = true;
					}
				}

				else if(this.board[rankInt2][sum2].piece!=null || this.board[rankInt2][sum3].piece != null){
					//System.out.println("in here");


					if(this.board[rankInt2][sum2].piece!=null){
						//System.out.println(this.board[rankInt2][sum2].piece.toString());
						this.board[rankInt2][sum2].piece.enpass = true;
					}
					else if(this.board[rankInt2][sum3].piece != null){
						//System.out.println(this.board[rankInt2][sum3].piece.toString());
						this.board[rankInt2][sum3].piece.enpass = true;
					}
				}
			}
		}
	}
	
	public void castle(int rankInt1, int fileInt1, int rankInt2, int fileInt2) {
		if (!this.pieceBlocking(rankInt1, fileInt1, rankInt2, fileInt2)) {
			if (fileInt1 > fileInt2) {
				if (this.board[rankInt1][fileInt1].isOccupied() && this.board[rankInt2][fileInt2 - 1].isOccupied()) {
					if (this.board[rankInt1][fileInt1].piece instanceof King && this.board[rankInt2][fileInt2 - 1].piece instanceof Rook) {
						if (!this.board[rankInt1][fileInt1].piece.hasMoved && !this.board[rankInt2][fileInt2 - 1].piece.hasMoved) {
							this.board[rankInt1][fileInt1 - 2].piece = this.board[rankInt1][fileInt1].piece;
							this.board[rankInt1][fileInt1].piece = null;
							
							this.board[rankInt2][fileInt2 + 1].piece = this.board[rankInt2][fileInt2 - 1].piece;
							this.board[rankInt2][fileInt2 - 1].piece = null;
							this.moveNumber++;
							this.blackTurn = (moveNumber % 2 == 0);
						} else
							System.out.println("Cannot castle. Piece already moved.");
					} else
					this.checkMove(rankInt1, fileInt1, rankInt2, fileInt2);
				}
			} else {
				if (this.board[rankInt1][fileInt1].isOccupied() && this.board[rankInt2][fileInt2 + 1].isOccupied()) {
					if (this.board[rankInt1][fileInt1].piece instanceof King && this.board[rankInt2][fileInt2 + 1].piece instanceof Rook) {
						if (!this.board[rankInt1][fileInt1].piece.hasMoved && !this.board[rankInt2][fileInt2 + 1].piece.hasMoved) {
							this.board[rankInt1][fileInt1 + 2].piece = this.board[rankInt1][fileInt1].piece;
							this.board[rankInt1][fileInt1].piece = null;
							
							this.board[rankInt2][fileInt2 - 2].piece = this.board[rankInt2][fileInt2 + 1].piece;
							this.board[rankInt2][fileInt2 + 1].piece = null;
							this.moveNumber++;
							this.blackTurn = (moveNumber % 2 == 0);
						} else
							System.out.println("Cannot castle. Piece already moved.");
					} else
						this.checkMove(rankInt1, fileInt1, rankInt2, fileInt2);
				}
			}
		} else
			System.out.println("Illegal move, try again.");
	}
	
	public void turnMessage() {
		System.out.print(this.blackTurn ? "\nBlack's turn: " : "\nWhite's turn: ");
	}
	
	public String toString() {
		String str = "";

		for (int i = board.length - 1; i >= 0; i--) {
			for (int j = board[i].length - 1; j >= 0 ; j--) {
					str += this.board[i][j].toString() + " ";
			}
			str += (i + 1) + "\n";
		}

		str += " a  b  c  d  e  f  g  h\n";

		return str;
	}
	
	public void draw() {
		this.gameEnded = true;
		System.out.println("Game drawn.");
	}
	
	public void resign() {
		this.gameEnded = true;
		if (blackTurn) {
			System.out.println("White wins!");
		} else
			System.out.println("Black wins!");
	}
	
	// Without this, the program reads the files a->h as h->a. Meaning "a2 a4" would be "h2 h4" on the board. 
	// This method fixes that.
	private char reverseLetters(char file) {
		switch (file) {
			case 'a' : file = 'h'; break;
			case 'b' : file = 'g'; break;
			case 'c' : file = 'f'; break;
			case 'd' : file = 'e'; break;
			case 'e' : file = 'd'; break;
			case 'f' : file = 'c'; break;
			case 'g' : file = 'b'; break;
			case 'h' : file = 'a'; break;
		}
		return file;
	}
}
