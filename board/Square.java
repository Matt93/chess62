package board;

import pieces.Piece;

public class Square {
	private int xCoordinate;
	private int yCoordinate;
	public Piece piece;

	/**
	 * Chessboard is comprised of 64 squares. Each square has an xcoordinate, ycoordinate, and a piece
	 * (piece is null if there is no piece there)
	 * @param xCoordinate
	 * 			xcoordinate of square
	 * @param yCoordinate
	 * 			ycoordinate of square
	 * @param piece
	 * 			if there is no piece in the square it is null
	 */

	public Square(int xCoordinate, int yCoordinate, Piece piece){
		this.xCoordinate = yCoordinate;
		this.yCoordinate = xCoordinate;
		this.piece = piece;
	}

	/**
	 * checks to see if square is occupied
	 * @return
	 * 		true if piece is occupied, false if it isn't
	 */
	public boolean isOccupied(){
		return this.piece != null;
	}

	/**
	 * if the square is null add in "##" or an white space
	 */

	public String toString() {
		if (this.piece == null) {
			if ((this.yCoordinate % 2 == 0 && this.xCoordinate % 2 == 0) || (this.yCoordinate % 2 != 0 && this.xCoordinate % 2 != 0)) {
				return "  ";
			} else {
				return "##";
			}
		} else {
			return this.piece.toString();
		}
	}

	/**
	 * @return
	 * 		returns the xcoordinate of the square
	 */

	public int getXCoordinate() {
		return this.xCoordinate;
	}

	/**
	 * @return
	 * 		returns the ycoordinate of the square
	 */

	public int getYCoordinate() {
		return this.yCoordinate;
	}

}
