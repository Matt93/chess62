package chess;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import board.ChessBoard;

public class Chess {

	public static void main(String[] args) {
		ChessBoard board = new ChessBoard();
		Scanner in = new Scanner(System.in);
		//String fileName = "moves.txt";
		//String line;
		
		System.out.print(board.toString());
		
		/*try {
            FileReader fileReader = new FileReader(fileName);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {

        		//while (!board.gameEnded) {
        			
        			board.turnMessage();
        			//String input = in.nextLine();
        			board.takeInput(line);		
        			
        			if (!board.gameEnded) {
        				System.out.print("\n");
        				System.out.println(board.toString());
        			}
        			
        			
        		//}   

                    
            } bufferedReader.close(); 
            
			while (!board.gameEnded) {
				
				board.turnMessage();
				String input = in.nextLine();
				board.takeInput(input);		
				
				if (!board.gameEnded) {
					System.out.print("\n");
					System.out.println(board.toString());
				}
			}
		}
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");                  
        } */
		
		while (!board.gameEnded) {
			
			board.turnMessage();
			String input = in.nextLine();
			board.takeInput(input);		
			
			if (!board.gameEnded) {
				System.out.print("\n");
				System.out.println(board.toString());
			}
		} 
		in.close();
	}
}
